Appalling Mist
==============

This repository contains resources for the band *Appalling Mist* from Bargen.

This currently includes:

 * Guitar tabs (what Tinu plays)

Further material may be added in the future.
